from app import app, db, models
from flask import render_template, redirect, url_for, flash
from .forms import loginForm, registerForm, createForm
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required, current_user


@app.route('/')
@login_required
def home():
    ## pass the username and the most recent 20 posts to the home template
    return render_template('home.html', title = "Home", username = current_user.username, posts = models.Post.query.order_by(models.Post.id.desc()).limit(20).all() )

@app.route('/login', methods=['GET', 'POST'])
def login():
    #initialise the login form
    form = loginForm()
    #if the login form has been submitted
    if form.validate_on_submit():
        ## search for the specified user in the database
        getUser = models.User.query.filter_by(username = form.username.data).first()
        if getUser:
            ## check if the hashed passwords are the same
            if check_password_hash(getUser.password, form.password.data):
                ## log the user into the session
                login_user(getUser, remember = form.rememberMe.data)
                return redirect(url_for('home'))

        ## if they entered an invalid username or password
        flash('Incorrect Credentials. Please Try Again.')
        return redirect(url_for('login'))

    return render_template('login.html', title = "Login", form = form)

@app.route('/logout')
@login_required
def logout():
    ## log the user out of the session
    logout_user()
    return redirect(url_for('login'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    #initialise the registration form
    form = registerForm()

    ## if the register form has been submitted
    if form.validate_on_submit():
        ## check if the user already exists
        if models.User.query.filter_by(username = form.username.data).scalar() is not None:
            flash('This username is already in use. Please use a different one')
            return redirect(url_for('register'))
        ## create a new user model
        createUser = models.User(username = form.username.data, password = generate_password_hash(form.password.data, method="sha256"))
        ## add the user model to the database
        db.session.add(createUser)
        db.session.commit()
        return redirect(url_for('home'))

    return render_template('register.html', title = "Register", form = form)

@app.route('/create', methods=['GET', 'POST'])
@login_required
def create():
    form = createForm()
    ## if the post creation form has been submitted
    if form.validate_on_submit():
        ## create a new post model instance
        createPost = models.Post(title = form.title.data, content = form.content.data, author_id = current_user.get_id())
        ## add the post model to the database
        db.session.add(createPost)
        db.session.commit()
        return redirect(url_for('myposts'))

    return render_template('create.html', title = "Create", form = form)


@app.route('/myposts', methods=['GET'])
@login_required
def myposts():
    ## pass all posts by a specific user ID to the myposts template
    return render_template('myposts.html', title = "My Posts", posts = models.Post.query.filter_by( author_id = current_user.get_id() ).all())

@app.route('/delete/<post_id>')
@login_required
def deletePost(post_id):
    ## search for the specific post in the db
    post = models.Post.query.filter_by(id = post_id).first()
    ## delete the post from the db
    db.session.delete(post)
    db.session.commit()
    return redirect(url_for('myposts'))
