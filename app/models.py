from app import db
from flask_login import UserMixin

## inherits from UserMixin to get methods needed for login
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(10), unique=True)
    password = db.Column(db.String(80))
    posts = db.relationship('Post', backref='author')

class Post(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(10))
    content = db.Column(db.String(200))
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'))
