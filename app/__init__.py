from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from logging import FileHandler, WARNING, ERROR

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
## initialise boostrap
Bootstrap(app)
migrate = Migrate(app,db)
## initialise the login manager
login_manager = LoginManager()
## set default login view
login_manager.login_view = 'login'
login_manager.login_message = 'You must login to access that page!'
login_manager.init_app(app)

file_handler_warnings = FileHandler('warning_log.txt')
file_handler_warnings.setLevel(WARNING)

file_handler_errors = FileHandler('error_log.txt')
file_handler_errors.setLevel(ERROR)

app.logger.addHandler(file_handler_warnings)
app.logger.addHandler(file_handler_errors)

from app import views, models

## method required by the login manager to return a user model from its id
@login_manager.user_loader
def load_user(user_id):
    return models.User.query.get(int(user_id))
