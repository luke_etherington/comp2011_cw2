from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Length

class loginForm(FlaskForm):
    username = StringField("Username", validators=[InputRequired(), Length(min=3, max=10)])
    password = PasswordField("Password", validators=[InputRequired(), Length(min=5, max=20)])
    rememberMe = BooleanField("Remember Me")

class registerForm(FlaskForm):
    username = StringField("Username", validators=[InputRequired(), Length(min=3, max=10)])
    password = PasswordField("Password", validators=[InputRequired(), Length(min=5, max=20)])

class createForm(FlaskForm):
    title = StringField("Title", validators=[InputRequired(), Length(min=1, max=10)])
    content = TextAreaField("Content", validators=[InputRequired(), Length(min=1, max=200)])
